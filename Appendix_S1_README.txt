Project name:
Phylogenetic and morphometric analysis of Plantago section Coronopus (Plantaginaceae)

Citation:
Höpke, J., Mucina, L. and Albach, D.C. (submitted) Phylogenetic and morphometric analysis of Plantago section Coronopus DC. (Plantaginaceae). Taxon.

Contact information regarding data analyses:
Jannes Höpke: jannes.hoepke@uni-oldenburg.de

Description of all data processing steps and analyses:
For a detailed description of data processing and analyses, please see the publication and the comments in the R script(s).

List of all files / Description for each file:
FOLDER “Supplementary_appendices”:
	Appendix S1. “Appendix_S1_README.txt”. Readme file with Information regarding all supplementary files.
	Appendix S2. “Appendix_S2_Hoepke_et_al_Morphometric_Analyses.R”. R-script for all morphometric data preparation and analyses.
	Appendix S3. “Appendix_S3_Hoepke_et_al_Morphometric_Analyses_RSessionInfo.txt”. Information about the R session, including all program and package versions from the last working R session.
	Appendix S4. “Appendix_S4_Hoepke_et_al_Morphometric_Raw_Data.csv”. All morphometric raw data.
	Appendix S5. “Appendix_S5_Hoepke_et_al_ITS_alignment.nex”. Alignment file for ITS.
	Appendix S6. “Appendix_S6_Hoepke_et_al_ITS_best.tre”. “Best” (= most likely) ITS tree resulting from GARLI.
	Appendix S7. “Appendix_S7_Hoepke_et_al_ITS_garli_consensus.tre”. 50% majority consensus ITS tree summarizing bootstrap replicates by GARLI.
	Appendix S8. “Appendix_S8_Hoepke_et_al_ITS_mb_consensus.tre”. 50% majority consensus ITS tree summarizing trees from the stationary phase in MrBayes.
	Appendix S9. “Appendix_S9_Hoepke_et_al_ITS_plotting.r”. R-script for plotting bootstrap support and posterior probabilities on the best ITS tree from GARLI.
	Appendix S10. “Appendix_S10_Hoepke_et_al_tree_plotting_RSessionInfo.txt”. Information about the R session, including all program and package versions from the last working R session for plotting ITS, trnL-F, and cpDNA files.
	Appendix S11. “Appendix_S11_Hoepke_et_al_ITS_bestTree_tips.csv”. Tip label information for plotting the ITS tree.
	Appendix S12. “Appendix_S12_Hoepke_et_al_trnLF_alignment_2018-04-10.nex”. Alignment file for trnL-F.
	Appendix S13. “Appendix_S13_Hoepke_et_al_trnLF_best_2018-04-10.tre”. “Best” (= most likely) trnL-F tree resulting from GARLI.
	Appendix S14. “Appendix_S14_Hoepke_et_al_trnLF_garli_consensus_2018-04-10.tre”. 50% majority consensus trnL-F tree summarizing bootstrap replicates by GARLI.
	Appendix S15. “Appendix_S15_Hoepke_et_al_trnLF_mb_consensus_2018-04-10.tre”. 50% majority consensus trnL-F tree summarizing trees from the stationary phase in MrBayes.
	Appendix S16. “Appendix_S16_Hoepke_et_al_trnLF_plotting.r”. R-script for plotting bootstrap support and posterior probabilities on the best trnL-F tree from GARLI.
	Appendix S17. “Appendix_S17_Hoepke_et_al_trnLF_bestTree_tips.csv”. Tip label information for plotting the trnL-F tree.
	Appendix S18. “Appendix_S18_Hoepke_et_al_cpDNA_alignment_2018-04-10.nex”. Alignment file for the combined cpDNA.
	Appendix S19. “Appendix_S19_Hoepke_et_al_cpDNA_best_2018-04-10.tre”. “Best” (= most likely) cpDNA tree resulting from GARLI.
	Appendix S20. “Appendix_S20_Hoepke_et_al_cpDNA_garli_consensus_2018-04-10.tre”. 50% majority consensus cpDNA tree summarizing bootstrap replicates by GARLI.
	Appendix S21. “Appendix_S21_Hoepke_et_al_cpDNA_mb_consensus_2018-04-10.tre”. 50% majority consensus cpDNA tree summarizing trees from the stationary phase in MrBayes.
	Appendix S22. “Appendix_S22_Hoepke_et_al_cpDNA_plotting.r”. R-script for plotting bootstrap support and posterior probabilities on the best cpDNA tree from GARLI.
	Appendix S23. “Appendix_S23_Hoepke_et_al_cpDNA_bestTree_tips.csv”. Tip label information for plotting the cpDNA tree.
FOLDER “Supplementary_figures”:
	Fig. S1. “Fig_S1A_ITS_carnosa.pdf” & “Fig_S1B_trnL-F_carnosa.pdf” Unrooted result of neighbour-net analysis of South African individuals of Plantago carnosa Lam. based on ITS (A) and trnL-F (B). Inland-populations circled.
	Fig. S2. “Fig_S2_Plantago_carnosa_distribution.jpg” Distribution map of Plantago carnosa Lam. in South Africa. Various morphological types (typical estuarine type, coastal-cliff type, inland hairy type) as well as genetically outlying inland types (Roggeveld type, Kleinzee [Namaqualand] type, Stormberg type) are featured.
	Fig. S3. “Fig_S3_LDA_revised.pdf” Linear discriminant analysis (LDA) for P. coronopus L. and P. crassifolia Forssk. from the Mediterranean region and P. carnosa Lam. from South Africa based on morphometric data. The calculation was done on all non-redundant characters from the standardised numeric dataset.
	Fig. S4. “Fig_S4_classification_trees_revised.pdf” Classification trees based on morphometric data. Calculated for all characters from the mixed dataset. Every node shows the conditions for specific variables that have to be fulfilled if a condition is met the left branch and if not the right one is taken. All shown numeric values are in millimetres, except for ratios. A, Pruned 1 SE rule tree for all five recognised species from our molecular analyses (P. carnosa Lam., P. coronopus L. [incl. P. cupanii Guss. and P. macrorhiza Poir.], P. crassifolia Forssk., P. crypsoides Boiss., P. serraria L.; missing P. asphodeloides Svent. and P. subspathulata Pilg. according to Rahn, 1996) of Plantago sect. Coronopus DC. (2.1% misclassification). If also the other four species (8 individuals) from outside this section were included, the tree does not change except for that they become included as misclassifications (9.6% misclassification) and that the node between P. crassifolia (cra) and P. serraria (ser) changes from its current splitting rule to “leaf lobe l < 1.8”; B, 1 SE rule pruned tree for P. carnosa/P. crassifolia (car/cra) and P. coronopus (cor) (0% misclassification); C, Tree forced to only one split for P. carnosa/P. crassifolia and P. coronopus (3.5% misclassification); D, 1 SE rule pruned tree for P. carnosa (car) and P. crassifolia (cra) (0% misclassification); E, Tree forced to only one split for P. carnosa and P. crassifolia (13.2% misclassification); F, 1 SE rule pruned tree for P. coronopus form the Mediterranean/Europe (cor_M) and South Africa (cor_S) (9.4% misclassification); G, 1 SE rule pruned tree for P. coronopus (cor) and P. crypsoides (cry) (2.6% misclassification). — Abbreviations are: cln = cauline, cl = cauline to basal rosette, l = length, w = width, lw = length to width.
	Fig. S5. “Fig_S5_morphometric_variables_revised.pdf” Pairwise comparisons of groups for the best differentiating variables as suggested by PCoA, LDA and classification tree importance. A–D, Between P. carnosa Lam./crassifolia Forssk. (= car/cra) and P. coronopus L. (= cor); E–L, Between P. carnosa (= car) and P. crassifolia (= cra); M–R, Between P. coronopus form the Mediterranean (= cor_M) and from South Africa (= cor_S); S–W, Between P. coronopus (= cor) and P. crypsoides Boiss. (= cry). — Abbreviations: l = length, w = width, and lw = length to width.
FOLDER “Supplementary_tables”:
	Table S1. “Table_S1_PCR_information.doc” List of primers, reaction mixes and PCR-programs used in the molecular analyses.
	Table S2. “Table_S2_character_definitions.doc” List of character definitions for directly measured characters.

